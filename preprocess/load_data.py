import pandas as pd
import numpy as np
import matplot.corelate as plot
import matplotlib.pyplot as plt
import console.console as cn
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.externals import joblib


df = pd.read_csv("../data/pima-data.csv")

cn.print_all()

print("Shape of data frame: \n", df.shape)
print("Head : \n", df.head(5))

print("Is there any NULL values: ", df.isnull().values.any())

#plot.plot_corr(df, 15, True)

print(df.corr())

# dropping the column skin
del df['skin']

df.head(5)

print(df.corr())

def bool_to_num(df, column):
    bool_to_num_map = {True : 1, False : 0}
    df[column] = df[column].map(bool_to_num_map)
    return df

df = bool_to_num(df, 'diabetes')

print(df.head())

num_true = len(df.loc[df['diabetes']==True])
num_false = len(df.loc[df['diabetes']==False])

print("\n The number of diabetes are ", num_true , " & it's percentage : ", (num_true/(num_true + num_false)) * 100)
print("\n The number of non-diabetes are ", num_false , " & it's percentage : ", (num_false/(num_true + num_false)) * 100)

'''
Spliting the preprocessed data to training data & test data
'''

feature_col_names = ['num_preg', 'glucose_conc', 'diastolic_bp', 'thickness', 'insulin', 'bmi', 'diab_pred', 'age']
predicted_class_names = ['diabetes']
X = df[feature_col_names].values     # predictor feature columns (8 X m)
#print('X \n', X)
y = df[predicted_class_names].values # predicted class (1=true, 0=false) column (1 X m)
#print('Y \n', y)
split_test_size = 0.30

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split_test_size, random_state=42)
# test_size = 0.3 is 30%, 42 is the answer to everything

print("{0:0.2f}% in training set".format((len(X_train)/len(df.index)) * 100))
print("{0:0.2f}% in test set".format((len(X_test)/len(df.index)) * 100))

# Verifying predicted value was split correctly

print("Original True  : {0} ({1:0.2f}%)".format(len(df.loc[df['diabetes'] == 1]), (len(df.loc[df['diabetes'] == 1])/len(df.index)) * 100.0))
print("Original False : {0} ({1:0.2f}%)".format(len(df.loc[df['diabetes'] == 0]), (len(df.loc[df['diabetes'] == 0])/len(df.index)) * 100.0))
print("")
print("Training True  : {0} ({1:0.2f}%)".format(len(y_train[y_train[:] == 1]), (len(y_train[y_train[:] == 1])/len(y_train) * 100.0)))
print("Training False : {0} ({1:0.2f}%)".format(len(y_train[y_train[:] == 0]), (len(y_train[y_train[:] == 0])/len(y_train) * 100.0)))
print("")
print("Test True      : {0} ({1:0.2f}%)".format(len(y_test[y_test[:] == 1]), (len(y_test[y_test[:] == 1])/len(y_test) * 100.0)))
print("Test False     : {0} ({1:0.2f}%)".format(len(y_test[y_test[:] == 0]), (len(y_test[y_test[:] == 0])/len(y_test) * 100.0)))

# Print missing data

print("# rows in dataframe {0}".format(len(df)))
print("# rows missing glucose_conc: {0}".format(len(df.loc[df['glucose_conc'] == 0])))
print("# rows missing diastolic_bp: {0}".format(len(df.loc[df['diastolic_bp'] == 0])))
print("# rows missing thickness: {0}".format(len(df.loc[df['thickness'] == 0])))
print("# rows missing insulin: {0}".format(len(df.loc[df['insulin'] == 0])))
print("# rows missing bmi: {0}".format(len(df.loc[df['bmi'] == 0])))
print("# rows missing diab_pred: {0}".format(len(df.loc[df['diab_pred'] == 0])))
print("# rows missing age: {0}".format(len(df.loc[df['age'] == 0])))

# impute missing value

# Impute with mean all 0 readings
fill_0 = SimpleImputer(missing_values=0, strategy="mean")

X_train = fill_0.fit_transform(X_train)
X_test = fill_0.fit_transform(X_test)

# Navie Bayes
# create Gaussian Naive Bayes model object and train it with the data
nb_model = GaussianNB()

nb_model.fit(X_train, y_train.ravel())

# Performance on Training Data

# predict values using the training data
nb_predict_train = nb_model.predict(X_train)
# Accuracy
print("Accuracy on training data: {0:.4f}".format(metrics.accuracy_score(y_train, nb_predict_train)))
print()
# predict values using the testing data
nb_predict_test = nb_model.predict(X_test)


# training metrics
print("Accuracy on test data: {0:.4f}".format(metrics.accuracy_score(y_test, nb_predict_test)))
print("")

# Metrics
print("Confusion Matrix")
print("{0}".format(metrics.confusion_matrix(y_test, nb_predict_test)))
print("")

print("Classification Report")
print(metrics.classification_report(y_test, nb_predict_test))


# Random forest

rf_model = RandomForestClassifier(random_state=42)      # Create random forest object
rf_model.fit(X_train, y_train.ravel())

# Accuracy of random forest in Training data

rf_predict_train = rf_model.predict(X_train)
# training metrics
print("Accuracy of random forest in Training data : {0:.4f}".format(metrics.accuracy_score(y_train, rf_predict_train)))

# Accuracy of random forest in test data

rf_predict_test = rf_model.predict(X_test)

# training metrics
print("Accuracy of random forest in test data: {0:.4f}".format(metrics.accuracy_score(y_test, rf_predict_test)))
# Confusion matrix
print(metrics.confusion_matrix(y_test, rf_predict_test) )
print("")
print("Classification Report")
print(metrics.classification_report(y_test, rf_predict_test))

# Logistic Regression

lr_model = LogisticRegression(C=0.7, random_state=42)
lr_model.fit(X_train, y_train.ravel())
lr_predict_test = lr_model.predict(X_test)

# training metrics
print("Accuracy in logistic regression on test data: {0:.4f}".format(metrics.accuracy_score(y_test, lr_predict_test)))
print(metrics.confusion_matrix(y_test, lr_predict_test))
print("")
print("Classification Report")
print(metrics.classification_report(y_test, lr_predict_test))

# Regularization paramaeter

C_start = 0.1
C_end = 5
C_inc = 0.1

C_values, recall_scores = [], []

C_val = C_start
best_recall_score = 0
while (C_val < C_end):
    C_values.append(C_val)
    lr_model_loop = LogisticRegression(C=C_val, random_state=42)
    lr_model_loop.fit(X_train, y_train.ravel())
    lr_predict_loop_test = lr_model_loop.predict(X_test)
    recall_score = metrics.recall_score(y_test, lr_predict_loop_test)
    recall_scores.append(recall_score)
    if (recall_score > best_recall_score):
        best_recall_score = recall_score
        best_lr_predict_test = lr_predict_loop_test

    C_val = C_val + C_inc

best_score_C_val = C_values[recall_scores.index(best_recall_score)]
print("1st max value of {0:.3f} occured at C={1:.3f}".format(best_recall_score, best_score_C_val))

plt.plot(C_values, recall_scores, "-")
plt.xlabel("C value")
plt.ylabel("recall score")
plt.show()

# Logistic regression with regularization parameter
C_start = 0.1
C_end = 5
C_inc = 0.1

C_values, recall_scores = [], []

C_val = C_start
best_recall_score = 0
while (C_val < C_end):
    C_values.append(C_val)
    lr_model_loop = LogisticRegression(C=C_val, class_weight="balanced", random_state=42)
    lr_model_loop.fit(X_train, y_train.ravel())
    lr_predict_loop_test = lr_model_loop.predict(X_test)
    recall_score = metrics.recall_score(y_test, lr_predict_loop_test)
    recall_scores.append(recall_score)
    if (recall_score > best_recall_score):
        best_recall_score = recall_score
        best_lr_predict_test = lr_predict_loop_test

    C_val = C_val + C_inc

best_score_C_val = C_values[recall_scores.index(best_recall_score)]
print("1st max value of {0:.3f} occured at C={1:.3f}".format(best_recall_score, best_score_C_val))

plt.plot(C_values, recall_scores, "-")
plt.xlabel("C value")
plt.ylabel("recall score")
plt.show()


# K -Fold cross validation
lr_cv_model = LogisticRegressionCV(n_jobs=-1, random_state=42, Cs=3, cv=10, refit=False, class_weight="balanced")  # set number of jobs to -1 which uses all cores to parallelize
lr_cv_model.fit(X_train, y_train.ravel())
# Predict on test data
lr_cv_predict_test = lr_cv_model.predict(X_test)

# training metrics
print("Accuracy: {0:.4f}".format(metrics.accuracy_score(y_test, lr_cv_predict_test)))
print(metrics.confusion_matrix(y_test, lr_cv_predict_test) )
print("")
print("Classification Report")
print(metrics.classification_report(y_test, lr_cv_predict_test))


# Save the trained model
joblib.dump(lr_cv_model, "../data/pima-trained-model.pkl")
# Load trained model
lr_cv_model = joblib.load("../data/pima-trained-model.pkl")

#
# get data from truncated pima data file
df_predict = pd.read_csv("../data/pima-data-trunc.csv")
print(df_predict.shape)
print("Pridict\n", df_predict)
del df_predict['skin']
print(df_predict)

X_predict = df_predict
del X_predict['diabetes']

#Impute with mean all 0 readings
fill_0 = SimpleImputer(missing_values=0, strategy="mean")
X_predict = fill_0.fit_transform(X_predict)

print(lr_cv_model.predict(X_predict))