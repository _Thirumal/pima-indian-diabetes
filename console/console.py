import pandas as pd
import numpy as np


def print_all(desired_width = 500, max_column=100):
    # print all output
    pd.set_option('display.width', desired_width)
    np.set_printoptions(linewidth=desired_width)
    pd.set_option('display.max_columns', max_column)

